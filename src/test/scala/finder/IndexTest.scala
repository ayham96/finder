package finder

import org.scalatest._

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, _}
import scala.concurrent.duration._
import scala.language.postfixOps

class IndexTest extends FlatSpec with Matchers {
  "Index.search" should "return empty list when no results found" in {
    Index(List()).search("foo") shouldEqual List()
  }

  "Index.search" should "should return 100 if all words exists" in {
    // Arrange
    val exampleDocs = List(
      Document("doc1", "lorem foo ipsum"),
      Document("doc2", "lorem foo Bar ipsum"),
      Document("doc3", "lorem baz-bar foo ipsum"),
    )
    val index = Index(exampleDocs)

    // Act
    val results = index.search("foo")

    // Assert
    results.size shouldEqual 3
    results.forall(m => m.rank == 100) shouldEqual true
  }

  "Index.search" should "should return value between 0 and 100 if some words exists" in {
    // Arrange
    val exampleDocs = List(
      Document("doc1", "lorem foo ipsum"),
      Document("doc2", "lorem foo Bar ipsum"),
      Document("doc3", "lorem baz-bar foo ipsum"),
    )
    val index = Index(exampleDocs)

    // Act
    val results = index.search("foo bar baz qux")

    // Assert
    results.size shouldEqual 3
    results.forall(m => 0 < m.rank && m.rank < 100) shouldEqual true
    results.map(m => m.docId) shouldEqual List("doc3", "doc2", "doc1")
  }
}

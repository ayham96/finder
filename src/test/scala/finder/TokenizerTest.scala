package finder

import org.scalatest._

import scala.language.postfixOps

class TokenizerTest  extends FlatSpec with Matchers {
  "Tokenizer.tokenize" should "create correct tokens from string" in {
    Tokenizer.tokenize("  \n \t ") shouldEqual List()
    Tokenizer.tokenize("Foo-bar  ,baz  .QUX") shouldEqual List("foo", "bar", "baz", "qux")
    Tokenizer.tokenize("foo\tbar\nbaz   123") shouldEqual List("foo", "bar", "baz", "123")
  }
}

package finder

import org.scalatest._

import scala.collection.mutable
import scala.concurrent.Future
import scala.concurrent._
import scala.concurrent.duration._
import ExecutionContext.Implicits.global
import scala.language.postfixOps

class ProgramTest extends FlatSpec with Matchers {
  class IOSpy(fakeInput: List[String]) extends ReadWriter {
    private var log = mutable.ListBuffer[String]()
    private var inIdx = -1

    override def read(): String = {
      inIdx += 1
      if (inIdx >= fakeInput.size) "" else fakeInput(inIdx)
    }

    override def write(out: String): Unit = {
      log += out
    }

    def getLogs(): List[String] = log.toList
  }

  "Program" should "terminate when :quit is entered" in {
    // Arrange
    lazy val terminated = Future {
      val program = new Program(new IOSpy(List(":quit")))
      // Act
      program.iterate(Index(List()))
      true
    }

    // Assert
    Await.result(terminated, 1 second) shouldEqual true
  }


  "Program" should "output not found when no matches are found" in {
    // Arrange
    val ioSpy = new IOSpy(List("foo" ,":quit"))
    val program = new Program(ioSpy)

    // Act
    program.iterate(Index(List()))

    // Assert
    ioSpy.getLogs() shouldEqual  List("search>", "no matches found", "search>")
  }

  "Program" should "display ranked matches" in {
    // Arrange
    val ioSpy = new IOSpy(List("one Two three four" ,":quit"))
    val program = new Program(ioSpy)
    val testIndex = Index(List(
      Document("25.txt", "bar One-baz"),
      Document("50.txt", "bar two One-baz"),
      Document("75.txt", "bar One-baz two, three."),
      Document("100.txt", "bar One-baz two, three. fOur."),
    ))

    // Act
    program.iterate(testIndex)

    // Assert
    ioSpy.getLogs() shouldEqual List("search>",
      "Results:",
      "100.txt : 100%\n75.txt : 75%\n50.txt : 50%\n25.txt : 25%",
      "search>")
  }

  "Program.readDocuments" should "read files in directory and return a list of Document" in {
    // Arrange
    val ioSpy = new IOSpy(List("one Two three four" ,":quit"))
    val program = new Program(ioSpy)
    val testDataPath = getClass.getResource("/testdata").getPath

    // Act
    val maybeDocs = program.readDocuments(Array(testDataPath))

    // Assert
    maybeDocs.isRight shouldEqual true

    val docs = maybeDocs.getOrElse(List())
    docs.size shouldEqual 2
    docs shouldEqual List(
      Document("f1.txt", "this is the first file"),
      Document("f2.txt", "this is the second file")
    )
  }
}

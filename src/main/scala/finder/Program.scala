package finder

import java.io.File
import java.nio.file.Files

import scala.util.Try

class Program(readWriter: ReadWriter) {
  sealed trait ReadFileError

  case object MissingPathArg extends ReadFileError
  case class NotDirectory(error: String) extends ReadFileError
  case class FileNotFound(t: Throwable) extends ReadFileError
  case class ReadIOException(t: Throwable) extends ReadFileError

  private def openDir(path: String): Either[ReadFileError, File] = {
    Try(new java.io.File(path))
      .fold(
        throwable => Left(FileNotFound(throwable)),
        dir =>
          if (dir.isDirectory) Right(dir)
          else Left(NotDirectory(s"Path [$path] is not a directory"))
      )
  }

  private def toDocuments(files: List[File]) : Either[ReadFileError, List[Document]] = {
    Try(files.map(f => Document(f.getName, Files.readString(f.toPath))))
      .fold(
        throwable => Left(ReadIOException(throwable)),
        docs => Right(docs)
      )
  }

  def readDocuments(args: Array[String]): Either[ReadFileError, List[Document]] = {
    for {
      path <- args.headOption.toRight(MissingPathArg)
      dir <- openDir(path)
      docs <- toDocuments(dir.listFiles(_.getName.endsWith(".txt")).toList)
    } yield docs
  }

  def iterate(indexedFiles: Index): Unit = {
    readWriter.write("search>")

    val searchString = readWriter.read().trim()
    if (searchString == ":quit") return

    val results = indexedFiles.search(searchString).map(x => s"${x.docId} : ${x.rank}%").mkString("\n")
    if (results == "") {
      readWriter.write("no matches found")
    } else {
      readWriter.write("Results:")
      readWriter.write(results)
    }

    iterate(indexedFiles)
  }
}


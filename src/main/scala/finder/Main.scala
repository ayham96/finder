package finder

object Main extends App {
  val program: Program = new Program(new StdReadWriter())

  program
    .readDocuments(args)
    .fold(
      println,
      docs => program.iterate(Index(docs))
    )
}


package finder

trait ReadWriter {
  def read():String
  def write(out: String): Unit
}


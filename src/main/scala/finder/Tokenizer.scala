package finder

object Tokenizer {
  def tokenize(s : String): List[String] = {
    s.toLowerCase.split("\\W+").toList
  }
}

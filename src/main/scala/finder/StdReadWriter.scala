package finder

import scala.io.StdIn.readLine

class StdReadWriter extends ReadWriter {
  override def read(): String = readLine()
  override def write(s: String): Unit = println(s)
}


package finder

case class Index(docs: List[Document]) {
  private val tokenToDocIds = docs
    .flatMap(doc => Tokenizer.tokenize(doc.body)
      .map(token => (token, doc.id))
    )
    .groupBy(_._1)
    .mapValues(tokenDocIdPairs => tokenDocIdPairs.map(_._2).toSet)
    .withDefaultValue(Set[String]())

  case class Match(docId: String, rank: Int)

  def search(searchString: String, limit: Int = 10): List[Match] = {
    val tokens = Tokenizer.tokenize(searchString)

    tokens.flatMap(token => tokenToDocIds(token))
      .groupBy(doc => doc)
      .toList
      .map(docAndMatches => Match(docAndMatches._1, (docAndMatches._2.length.toFloat / tokens.length * 100).toInt))
      .sortBy(x => -x.rank)
      .take(limit)
  }
}
